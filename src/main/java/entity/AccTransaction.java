package entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

//Phần tử (element) name của @Entity là không bắt buộc.
//Entity khớp với một bảng lấy theo tên theo thứ tự ưu tiên:
//1 - name trong @Table
//2 - name trong @Entity
//3 - name của class.
//Việc chỉ định rõ name của @Entity cho phép viết ngắn câu HSQL

@Entity(name="AccTransaction")
@Table(name = "ACC_TRANSACTION")
public class AccTransaction implements Serializable {
	
}
