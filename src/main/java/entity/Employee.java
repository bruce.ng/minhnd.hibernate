package entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import common.Department;

//@Id tham gia vào chú thích ID của Entity, nó tương đương với việc hiểu rằng cột đó là khóa chính của bảng (Primary Key).

@Entity
@Table(name = "EMPLOYEE")
public class Employee implements Serializable {
	
	private Integer empId;
	private String firstName;
	private Float pendingBalance;
	private String articleContent;
	private byte[] imageValue;
	private Date startDate;
	private Date fundsAvailDate;
	
	// @Id chú thích đây là id của Entity.
	// Và EMP_ID chính là khóa chính (Primary Key) của bảng.
	
	//Chú thích @GeneratedValue tương đương với
	//@GeneratedValue(strategy=GenerationType.AUTO)
	
	@Id
	@GeneratedValue
	@Column(name = "EMP_ID")
	public Integer getEmpId() {
		return empId;
	}
	
	/**
	 * @GeneratedValue(strategy= AUTO) sẽ được gán giá trị tự động, giá trị đó có thể từ SEQUENCE hoặc
	 * tự tạo ra do cơ chế IDENTITY của cột. Nó phụ thuộc vào loại DB.
	 * Với Oracle nó sẽ gọi từ Sequence Hibernate_Sequence để tạo ra một giá trị tăng dần gán vào cho cột.
	 * Với các DB khác chẳng hạn như MySQL, DB2, SQL Server, Sybase và Postgres cột có thể là IDENTITY
	 * và giá trị của nó có thể tự tăng.
	 * 
	 * @GeneratedValue(strategy=GenerationType.IDENTITY)
	 * Cột IDENTITY chỉ được support bởi một vài Database không phải là tất cả, ví dụ MySQL, DB2, SQL Server, Sybase và  Postgres.
	 * Oracle không hỗ trợ cột kiểu này.
	 * 
	 * @GeneratedValue(strategy=GenerationType.SEQUENCE)
	 * SEQUENCE là một đối tượng database, lưu trữ một giá trị tăng dần sau mỗi lần gọi lấy giá trị tiếp theo, và được hỗ trợ bởi Oracle, DB2,
	 * and Postgres.
	 * 
	 * 
	 * */
	
	//Đây là một cột kiểu chuỗi, vì thế length luôn có ý nghĩa và cần thiết
	//nullable mặc định là true
	//length mặc định là 255
	
	@Column(name = "FIRST_NAME", length = 20, nullable = false)
	public String getFirstName() {
		return firstName;
	}
	
	//@Column không chỉ rõ phần tử length, mặc định nó là 255.
	
	@Column(name = "DESCRIPTION", nullable = true)
	public String getDescription() {
		return firstName;
	}
	
	//Với các cột kiểu số hoặc Date bạn có thể bỏ qua length
	//(Nó không có ý nghĩa trong trường hợp này).
	
	@Column(name = "PENDING_BALANCE")
	public Float getPendingBalance() {
		return pendingBalance;
	}
	
	//Chú ý rằng trong một số Database có phân biệt TINY, MEDIUM, LARGE BLOB/CLOB.
	//Còn một số database thì không.
	//Phần tử length trong @Column trong trường hợp này sẽ quyết định nó map vào BLOB/CLOB nào.
	//Trong trường hợp cho phép BLOB/CLOB tối đa hãy để length = Integer.MAX_VALUE
	
	//Method này trả về byte[]
	//@Lob trong trường hợp này chú thích cho cột BLOB
	@Lob
	@Column(name = "IMAGE_VALUE", nullable = true, length = Integer.MAX_VALUE)
	public byte[] getImageValue() {
		return this.imageValue;
	}
	
	//Method này trả về String
	//@Lob trong trường hợp này sẽ chú thích cho CLOB.
	@Lob
	@Column(name = "ARTICLE_CONTENT", nullable = true, length = Integer.MAX_VALUE)
	public String getArticleContent() {
		return this.articleContent;
	}
	
	//@Temporal sử dụng chú thích cho cột có kiểu dữ liệu ngày tháng.
	//Có 3 giá trị cho TemporalType:
	//1 - TemporalType.DATE
	//2 - TemporalType.TIME
	//3 - TemporalType.TIMESTAMP
	
	@Temporal(TemporalType.DATE)
	@Column(name = "START_DATE", nullable = false)
	public Date getStartDate() {
		return startDate;
	}
	
	//TemporalType.DATE chú thích cột sẽ lưu trữ ngày tháng năm (bỏ đi thời gian)
	//TemporalType.TIME chú thích cột sẽ lưu trữ thời gian (Giờ phút giây)
	//TemporalType.TIMESTAMP chú thích cột sẽ lưu trữ ngày tháng và cả thời gian
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FUNDS_AVAIL_DATE", nullable = false)
	public Date getFundsAvailDate() {
		return fundsAvailDate;
	}
	
	
	/**
	 * @OneToMany là cách chú thích để lấy danh sách các bản ghi con của bản ghi hiện tại
	 * trong quan hệ 1 nhiều. Nó là đảo của chú thích @ManyToOne,
	 * và vì vậy nó dựa vào chú thích @ManyToOne để định nghĩa ra @OneToMany.
	 * */
	private Department department;
	private String	lastName;
	private Object	manager;
  
  // Quan hệ N-1 (Nhiều - Một) định nghĩa department.

  @JoinColumn(name = "DEPT_ID", nullable = true, foreignKey = @ForeignKey(name = "EMPLOYEE_DEPARTMENT_FK"))
	@ManyToOne(fetch = FetchType.LAZY)
	public Department getDepartment() {
		return department;
	}
  
  //@Transient để chú thích cho các getter method mà bạn muốn Hibernate bỏ qua method này
	@Transient
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}
	
	@Transient
	public boolean isManagerEmployee() {
		return this.manager != null;
	}

}
