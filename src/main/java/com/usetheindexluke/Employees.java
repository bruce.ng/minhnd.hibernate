package com.usetheindexluke;

/* "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License. 
 * 
 *                     http://Use-The-Index-Luke.com/
 */
import java.util.ArrayList;
import java.util.Date;
import java.util.Collection;
import java.io.*;
import javax.persistence.*;

@Entity
@Table(name = "employees")
public class Employees implements Serializable, EmployeesHeadInterface {
  /*
   * Expects the Employees tables a created in Use The Index, Luke:
   * 
   * http://use-the-index-luke.com/sql/example-schema/oracle/where-clause
   * http://use-the-index-luke.com/sql/example-schema/postgresql/where-clause
   * http://use-the-index-luke.com/sql/example-schema/sql-server/where-clause
   */
  private static final long serialVersionUID = 2020362434357776023L;
  
  @Column(name = "first_name")
  private String firstName;
  @Column(name = "last_name")
  private String lastName;
  @Id
  @Column(name = "employee_id")
  private long employeeId;
  @Id
  @Column(name = "subsidiary_id")
  private long subsidiaryId;
  @Temporal(TemporalType.DATE)
  @Column(name = "date_of_birth")
  @Basic(fetch = FetchType.LAZY)
  private Date dateOfBirth;
  @Column(name = "phone_number")
  @Basic(fetch = FetchType.LAZY)
  private String phoneNumber;
  @Column(name = "junk")
  @Basic(fetch = FetchType.LAZY)
  private String junk;
  @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
  /* use FetchType.EAGER alternatively */
  private Collection<Sales> sales = new ArrayList<Sales>();
  
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  
  public String getFirstName() {
    return firstName;
  }
  
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  
  public String getLastName() {
    return lastName;
  }
  
  public void setEmployeeId(long employeeId) {
    this.employeeId = employeeId;
  }
  
  public long getEmployeeId() {
    return employeeId;
  }
  
  public void setSubsidiaryId(long subsidiaryId) {
    this.subsidiaryId = subsidiaryId;
  }
  
  public long getSubsidiaryId() {
    return subsidiaryId;
  }
  
  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }
  
  public Date getDateOfBirth() {
    return dateOfBirth;
  }
  
  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }
  
  public String getPhoneNumber() {
    return phoneNumber;
  }
  
  public void setJunk(String junk) {
    this.junk = junk;
  }
  
  public String getJunk() {
    return junk;
  }
  
  public void setSales(Collection<Sales> sales) {
    this.sales = sales;
  }
  
  public Collection<Sales> getSales() {
    return sales;
  }
  
}
