package com.usetheindexluke;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.metamodel.SingularAttribute;

@javax.persistence.metamodel.StaticMetamodel(com.usetheindexluke.Sales.class)
public class Sales_ {
  public static volatile SingularAttribute<Sales, Long> saleId;
  public static volatile SingularAttribute<Sales, Employees> employee;
  /*
   * public static volatile SingularAttribute<Sales,Long> employeeId; public
   * static volatile SingularAttribute<Sales,Long> subsidiaryId;
   */
  public static volatile SingularAttribute<Sales, Date> saleDate;
  public static volatile SingularAttribute<Sales, BigDecimal> eurValue;
}
