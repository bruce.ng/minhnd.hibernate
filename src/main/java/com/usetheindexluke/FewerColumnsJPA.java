package com.usetheindexluke;

/**
 * Demonstrates the DTO/JP-QL approach to select only a few columns from a join.
 * http://use-the-index-luke.com/sql/join/hash-join-partial-objects
 */

/* "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License. 
 * 
 *                     http://Use-The-Index-Luke.com/
 */

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FewerColumnsJPA {
  public static void main(String[] args) {
    EntityManager em = Persistence.createEntityManagerFactory("usetheindexluke").createEntityManager();
    
    try {
      /* The "original" query, using entities */
      // Query q =
      // em.createQuery("from Sales s join fetch s.employee e where s.saleDate > :dt");
      
      /* A compatible query, using the lightweight DTO approach */
      Query q = em.createQuery("select new com.usetheindexluke.SalesHeadDTO" + "(s.saleDate, s.eurValue, e.firstName, e.lastName)"
          + " from Sales s join s.employee e where s.saleDate > :dt");
      q.setParameter("dt", getSixMonthAgo());
      
      List<SalesHeadInterface> sales = q.getResultList();
      for (SalesHeadInterface s : sales) {
        String txt = " Employee: " + s.getEmployee().getLastName() + " date: " + s.getSaleDate() + " EUR value: " + s.getEurValue();
        // System.out.println(txt);
      }
    } finally {
      em.close();
    }
  }
  
  // calculating the boundary date on the client side, avoiding
  // interoperability problems, especially since JP-QL doens't provide
  static Date getSixMonthAgo() {
    Calendar c = Calendar.getInstance();
    c.add(Calendar.MONTH, -6);
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    return c.getTime();
  }
}
