package com.usetheindexluke;

import java.util.Date;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.CollectionAttribute;

@javax.persistence.metamodel.StaticMetamodel(com.usetheindexluke.Employees.class)
public class Employees_ {
  public static volatile SingularAttribute<Employees, Long> employeeId;
  public static volatile SingularAttribute<Employees, Long> subsidiaryId;
  public static volatile SingularAttribute<Employees, String> firstName;
  public static volatile SingularAttribute<Employees, String> lastName;
  public static volatile SingularAttribute<Employees, String> phoneNumber;
  public static volatile SingularAttribute<Employees, Date> dateOfBirth;
  public static volatile SingularAttribute<Employees, String> junk;
  public static volatile CollectionAttribute<Employees, Sales> sales;
}
