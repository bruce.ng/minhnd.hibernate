package com.usetheindexluke;

/**
 * The Sales entity for the Join and FewerColumns samples.
 */

/* "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License. 
 * 
 *                     http://Use-The-Index-Luke.com/
 */

import java.math.BigDecimal;
import java.util.Date;
import java.io.*;
import javax.persistence.*;

@Entity
@Table(name = "sales")
public class Sales implements Serializable, SalesHeadInterface {
  /*
   * Expects the Sales table a created in Use The Index, Luke:
   * 
   * http://use-the-index-luke.com/sql/example-schema/oracle/join
   * http://use-the-index-luke.com/sql/example-schema/postgresql/join
   * http://use-the-index-luke.com/sql/example-schema/sql-server/join
   */
  private static final long serialVersionUID = 2020362434357776023L;
  
  @Id
  @Column(name = "sale_id")
  private long saleId;
  
  @Temporal(TemporalType.DATE)
  @Column(name = "sale_date")
  private Date saleDate;
  
  @Column(name = "eur_value")
  private BigDecimal eurValue;
  
  @ManyToOne
  @JoinColumns({ @JoinColumn(name = "employee_id", referencedColumnName = "employee_id"),
      @JoinColumn(name = "subsidiary_id", referencedColumnName = "subsidiary_id") })
  private Employees employee;
  
  @Column(name = "junk")
  @Basic(fetch = FetchType.LAZY)
  private String junk;
  
  public Sales() {
  }
  
  public void setSaleId(long saleId) {
    this.saleId = saleId;
  }
  
  public long getSaleId() {
    return saleId;
  }
  
  public void setSaleDate(Date saleDate) {
    this.saleDate = saleDate;
  }
  
  public Date getSaleDate() {
    return saleDate;
  }
  
  public void setEurValue(BigDecimal eurValue) {
    this.eurValue = eurValue;
  }
  
  public BigDecimal getEurValue() {
    return eurValue;
  }
  
  public void setEmployee(Employees employee) {
    this.employee = employee;
  }
  
  public Employees getEmployee() {
    return employee;
  }
  
  public void setJunk(String junk) {
    this.junk = junk;
  }
  
  public String getJunk() {
    return junk;
  }
}
