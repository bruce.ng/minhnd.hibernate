package com.usetheindexluke;

/* "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License. 
 * 
 *                     http://Use-The-Index-Luke.com/
 */

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
//import org.hibernate.ejb.Ejb3Configuration;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class JoinHibernate {
  public static void main(String[] args) {
    Session session = new Configuration().configure().buildSessionFactory().openSession();
    try {
      String lastName = "Win%";
      Criteria criteria = session.createCriteria(Employees.class);
      criteria.add(Restrictions.ilike("lastName", lastName));
      
      /*
       * // Uncomment to fetch via join criteria.setFetchMode("sales",
       * FetchMode.JOIN);
       * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
       */
      
      List<Employees> result = criteria.list();
      
      for (Employees e : result) {
        System.out.println("Id " + e.getEmployeeId() + " (" + e.getSubsidiaryId() + "): " + e.getFirstName() + " " + e.getLastName());
        for (Sales s : e.getSales()) {
          System.out.println("   SalesId: " + s.getSaleId() + " date: " + s.getSaleDate() + " EUR value: " + s.getEurValue());
        }
      }
    } finally {
      session.close();
    }
  }
}
