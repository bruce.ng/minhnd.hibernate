package com.usetheindexluke;

/* "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License. 
 * 
 *                     http://Use-The-Index-Luke.com/
 */

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import java.util.Iterator;
import java.util.List;

public class DynamicSQLHibernate {
  public static void main(String[] args) {
    Session session = new Configuration().configure().buildSessionFactory().openSession();
    try {
      Integer subsidiaryId = null, employeeId = null;
      String lastName = "Winand";
      Criteria criteria = session.createCriteria(Employees.class);
      if (subsidiaryId != null) {
        criteria.add(Restrictions.eq("subsidiaryId", subsidiaryId));
      }
      if (employeeId != null) {
        criteria.add(Restrictions.eq("employeeId", employeeId));
      }
      if (lastName != null) {
        criteria.add(Restrictions.eq("lastName", lastName).ignoreCase());
      }
      List results = criteria.list();
      
      for (Iterator it = results.iterator(); it.hasNext();) {
        Employees e = (Employees) it.next();
        System.out.println("Id " + e.getEmployeeId() + ": " + e.getFirstName() + " " + e.getLastName());
      }
    } finally {
      session.close();
    }
  }
}
