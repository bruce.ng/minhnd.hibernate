package com.usetheindexluke;

/**
 * This sample can be used to
 * -> Test non-instrumented load to select everything
 * -> Test byte-code instrumented property level lazy loading (junk columns)
 *    use the ant task "instrument" to make the magic work
 * -> test "fetch all properties" of HQL for lazy loaded properties.
 * 
 * http://docs.jboss.org/hibernate/core/3.6/reference/en-US/html/performance.html#performance-fetching-lazyproperties
 * http://use-the-index-luke.com/sql/join/hash-join-partial-objects
 */

/* "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License. 
 * 
 *                     http://Use-The-Index-Luke.com/
 */

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FewerColumnsInstrumentedHibernate {
  public static void main(String[] args) {
    Session session = new Configuration().configure().buildSessionFactory().openSession();
    try {
      String sales_fetch = ""; // " fetch all properties ";
      String employee_fetch = ""; // " fetch all properties ";
      Query q = session.createQuery("select s from Sales s " + sales_fetch + " inner join fetch s.employee e " + employee_fetch
          + " where s.saleDate >:dt");
      q.setParameter("dt", getSixMonthAgo());
      List<Sales> result = q.list();
      
      for (Sales s : result) {
        String txt = " Employee: " + s.getEmployee().getLastName() + " date: " + s.getSaleDate()
        // + " Junk: " + s.getJunk() // uncomment to see generated SQL
            + " EUR value: " + s.getEurValue();
        // System.out.println(txt);
      }
    } finally {
      session.close();
    }
  }
  
  // calculating the boundary date on the client side, avoiding
  // interoperability problems, especially since HQL doens't provide
  // INTERVAL calculations
  // http://opensource.atlassian.com/projects/hibernate/browse/HHH-2434
  static Date getSixMonthAgo() {
    Calendar c = Calendar.getInstance();
    c.add(Calendar.MONTH, -6);
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    return c.getTime();
  }
}
