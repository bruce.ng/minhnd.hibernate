package generationType.table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class Department {
	
	private Long deptId;

	@TableGenerator(
      name="deptGen",
      table="ID_GEN_TABLE",
      pkColumnName="KEY_COLUMN",
      valueColumnName="VALUE_COLUMN",
      pkColumnValue="DEPT_ID",
      allocationSize=1)
  @Id
  @GeneratedValue(strategy=GenerationType.TABLE, generator="deptGen")
  public Long getDeptId()  {
     return deptId;
  }
	
	//Chú ý: Bạn có thể tùy biến tên bảng, tên cột (ID_GEN_TABLE, KEY_COLUMN, VALUE_COLUMN)
}
