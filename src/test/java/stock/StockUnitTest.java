package stock;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.Test;

import persistence.HibernateUtils;
import common.Stock;

public class StockUnitTest {
	
	private static Log _log = LogFactory.getLog(StockUnitTest.class);
	
	//@Test
	public void insertStock() {
		_log.info("\nMaven + Hibernate + MySQL");
		//USE Configuration().configure().buildSessionFactory()
		Session session = HibernateUtils.getSessionFactory().openSession();
		
		session.beginTransaction();
		Stock stock = new Stock();
		
		stock.setStockCode("4715");
		stock.setStockName("GENM");
		
		session.save(stock);
		session.getTransaction().commit();
		session.close();
	}
	
	//@Test
	public void countStock() {
		//Use AnnotationConfiguration()
		SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		String SQL_QUERY = "SELECT COUNT(*) FROM Stock s";
		
		_log.info(SQL_QUERY);
		
		Query query = session.createQuery(SQL_QUERY);
		
		for (Iterator<?> it = query.iterate(); it.hasNext();) {
			long row = (Long) it.next();
			System.out.print("Count: " + row);
		}
		session.getTransaction().commit();
		session.close();
	}
	
}
